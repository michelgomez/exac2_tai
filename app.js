const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const misRutas = require("./router/index");
const path = require("path");

const app = express(); //OBJETO PRINCIPAL DE LA APLICACION
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public')); 
app.use(bodyparser.urlencoded({extended:true}));

//cambiar extensiones a ejs a html
app.engine("html", require("ejs").renderFile);

app.use(misRutas);


const puerto = 8080;  
app.listen(puerto, ()=>{    //La aplicacion va a escuchar por el puerto 3000
    console.log("Iniciando puerto");
});